

# Spa-example
Greetings! This is all part of section 16. A spring boot app that servers
java script pages. The java script app will use PCKE 

## Running the project

 - You can run this app for units 134 to 149. You  will need the following dependent projects running
    ```aidl
     1) ResourceServer version: Section12_unit90-94
     2) ApiGateway : version Section16
     3) discoveryservicelee: version main
    ```
 - For unit 150 we will not use an API Gateway and we will use the resource
 server to access the application directly
```aidl
    1)ResourceServer version: Section16 NOTE since resource server is running 
    on a random port you will have to look at the console output after the
    server starts.
```


## Realms
- Admin : leeioa/P0leta123
- Realm 2 bozo.cloun/P0leta123
http://localhost:8080/auth/realms/appsdeveloperlee/account/

